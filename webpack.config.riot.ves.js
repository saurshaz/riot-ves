var webpack = require('webpack')
var FlowStatusWebpackPlugin = require('flow-status-webpack-plugin')

module.exports = {
  devtool: 'cheap-module-source-map',
  entry: './src/riot/entry.js',

  module: {
    preLoaders: [{
      test: /\.js|\.html$/,
      exclude: /node_modules/,
      loader: 'riotjs-loader',
      query: {
        modules: 'common'
      }
    }],
    loaders: [{
      test: /\.js|\.html$/,
      exclude: /node_modules/,
      include: /client/,
      loader: 'riotjs-loader',
      query: {
        modules: 'common'
      }
    }, {
      test: /\.js|\.html$/,
      exclude: /node_modules/,
      include: /client/,
      loader: 'babel-loader',
      query: {
        modules: 'common'
      }
    }, // Font and images
      { test: /\.((woff2?|svg)(\?v=[0-9]\.[0-9]\.[0-9]))|(woff2?|svg|jpe?g|png|gif|ico)$/, loader: 'url?limit=10000' },
      { test: /\.((ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9]))|(ttf|eot)$/, loader: 'file' }, {
        test: /\.css$/,
        loader: 'style-loader!css-loader!postcss-loader'
      }],
    plugins: [
      // new webpack.HotModuleReplacementPlugin(),
      // hot reload
      // new webpack.IgnorePlugin(/\.json$/),
      new webpack.DefinePlugin({
        __DEVELOPMENT__: false,
        __QA__: true,
        __DEVTOOLS__: false,
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),
      new FlowStatusWebpackPlugin(),
       // hot reload
      new webpack.IgnorePlugin(/\.json$/),
      new webpack.optimize.CommonsChunkPlugin('main', 'main.js')
       // new ExtractTextPlugin("styles/main.css"),
      // new webpack.optimize.UglifyJsPlugin()
     // new webpack.ProvidePlugin({
     //   riot: 'riot'
     // })
    ]
  }
}
